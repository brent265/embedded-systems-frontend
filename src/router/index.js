import Vue from 'vue'
import Router from 'vue-router'
import Log from '@/components/Log'
import Status from '@/components/Status'
import Settings from '@/components/Settings'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Status
    },
    {
      path: '/log',
      name: 'log',
      component: Log,
      props: true
    },
    {
      path: '/status',
      name: 'status',
      component: Status
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings
    }
  ]
})
