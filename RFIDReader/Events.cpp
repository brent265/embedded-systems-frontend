#include <Arduino.h>
#include "RFID.h"
#include "Functions.h"

void messageReceived(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);

  byte CardNumberByte[4];
  unsigned long number = strtoul(string2char(payload), nullptr, 16);

  for (int i = 3; i >= 0; i--) // start with lowest byte of number
  {
    CardNumberByte[i] = number & 0xFF;  // or: = byte( number);
    number >>= 8;            // get next byte into position
  }

  if (topic == "/add") {
    writeID(CardNumberByte);
  }

  if (topic == "/reset") {
    resetROM();
  }

  if (topic == "/remove") {
    deleteID(CardNumberByte);
  }
}
