#include <SPI.h>
#include "RFID.h"
#include <EEPROM.h>
#include "Events.h"
#include <MFRC522.h>
#include "Functions.h"
#include <MQTTClient.h>
#include <ESP8266WiFi.h>

#define RST_PIN 5
#define SS_PIN 4
#define BUZZER_PIN 16
#define WIFI_SSID "Robert"
#define WIFI_PASSWORD "3372550337"
#define SHIFTR_USERNAME "dccdbecb"
#define SHIFTR_PASSWORD "402f8e6c39c9f4a9"

MFRC522 mfrc522(SS_PIN, RST_PIN);
MQTTClient mqttClient;
WiFiClient  wifiClient;

unsigned long lastMillis = 0;
byte readCard[4];   // Stores scanned ID read from RFID Module

void setup() {
  Serial.begin(9600);
  SPI.begin();
  mfrc522.PCD_Init();
  EEPROM.begin(512);

  connect(); // Connect to WiFi and MQTT

  mqttClient.onMessage(messageReceived); // Register Callback Event

  Serial.println(F("Scan for Card and print UID:"));
}

void connect() {
  Serial.println(F("Connecting to WiFi"));
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  int retries = 0;
  while ((WiFi.status() != WL_CONNECTED) && (retries < 10)) {
    retries++;
    delay(500);
    Serial.print(".");
  }
  
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println(F("WiFi connected"));
  }

  Serial.println(F("Connecting to MQTT Broker"));
  mqttClient.begin("broker.shiftr.io", wifiClient);

  while (!mqttClient.connect("arduino", SHIFTR_USERNAME, SHIFTR_PASSWORD)) {
    Serial.print(".");
    delay(1000);

    if (!wifiClient.connected()) {
      return;
    }
  }

  // Subscribe to channels
  mqttClient.subscribe("/add");
  mqttClient.subscribe("/remove");
  mqttClient.subscribe("/reset");
  Serial.println(F("Connected to MQTT Broker"));
}


void loop() {
  mqttClient.loop(); // Needed for callback

  if (!wifiClient.connected()) {
    connect();
  }

  // Send "up" signal every second
  if (millis() - lastMillis > 1000) {
    lastMillis = millis();
    mqttClient.publish("/up", "1");
  }

  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return;
  }

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return;
  }

  String cardId = "";

  for (byte i = 0; i < mfrc522.uid.size; i++) {
    readCard[i] = mfrc522.uid.uidByte[i];
    cardId += String(mfrc522.uid.uidByte[i] < 0x10 ? "0" : "");
    cardId += String(mfrc522.uid.uidByte[i], HEX);
  }

  // Debug function to print out card number to Serial Monitor
  printHex(readCard, 4);

  if (findID(readCard) ) { 
    Serial.println(F("Access Granted"));
    mqttClient.publish("/log", cardId + "-1");
    tone(BUZZER_PIN, 1000);
    delay(500);
    noTone(BUZZER_PIN);
  }
  else {
    mqttClient.publish("/log", cardId + "-0");
    Serial.println("Access Denied");
    tone(BUZZER_PIN, 250);
    delay(1000);
    noTone(BUZZER_PIN);
  }

  Serial.println();
  mfrc522.PICC_HaltA(); // Stop reading
}

