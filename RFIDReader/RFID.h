#ifndef RFID_H
#define RFID_H

void readID( uint8_t number );
bool checkTwo ( byte a[], byte b[] );
uint8_t findIDSLOT( byte find[]);
bool findID( byte find[]);
void writeID( byte a[] );
void deleteID( byte a[] );
void resetROM();

#endif
